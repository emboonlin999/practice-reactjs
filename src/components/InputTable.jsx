import React, { Component } from 'react'
import { Form,Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export default class InputTable extends Component {
  constructor(){
    super()
    this.state={
      enterValue:''
    }
  }
  handleOnchange=(item)=>{
    this.setState({enterValue:item})
  }
  handleOnclick =(item)=>{
    this.props.onAdd(this.state.enterValue)
  }

  render() {
    return (
      <div >
           <Form style={{ textAlign: 'center', marginTop:'50px'}} >
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label >Discripttions</Form.Label>
    <Form.Control type="Text" placeholder="Enter here " onChange={(d)=>this.handleOnchange(d.target.value)}/>
  </Form.Group >
  <Button variant="primary " onClick={this.handleOnclick} >
    Add_Data
  </Button>
</Form>
    </div>
    )
  }
}
