import React, { Component } from 'react'
import { Button, Table } from 'react-bootstrap'

export default class TableInfo extends Component {

 handleClickButton =(ent)=>{
     this.props.onChangeStatus(ent)
 }
  render() {
      const data=this.props.data
    return (

      <div>
   <Table striped bordered hover style={{marginTop:'20px'}} >
       <thead>
           <tr>
               <th>ID</th>
               <th>Title</th>
               <th>Status</th>
           </tr>
       </thead>
       <tbody>
           {
               data.map((item)=>(
                <tr key={item.id}>
                    <td> {item.id} </td>
                    <td> {item.title} </td>
                    <td> {  <Button variant="info" onClick={()=>this.handleClickButton(item.id)}>
                        {item.status}
                    </Button>} </td>
                  
                </tr>
              
                )

         )

           }
       </tbody>

   </Table>

      </div>
    )
  }
}
