import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import InputTable from './components/InputTable'
import TableInfo from './components/TableInfo'

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          title: " ReactJS is JavaScript library used for building reusable UI components.",
          status: 'predding ',

        },

        {
          id: 2,
          title: "Spring framework is an open source Java platform ",
          status: 'predding ',

        },

        {
          id: 3,
          title: " Bootstrap is the most popular CSS Framework for developing responsive and mobile-first websites",
          status: 'predding ',

        },
        {
          id: 4,
          title: " Java works on different platforms (Windows, Mac, Linux, Raspberry Pi, etc.) ",
          status: 'predding ',

        },
        {
          id: 5,
          title:"Every Web Developer must have a basic understanding of HTML, CSS, and JavaScript.",
          
          status: 'predding ',

        }





      ]
    }
  }

  onChangeStatus = (ent) => {
    let {data} = this.state
    let temp = data.map(item => item.id === ent ? {
      ...item, status:item.status === "predding " ? "done" : "predding"
    }:item)
    this.setState({data:temp})
  }

onAdd=(title) => {
  let {data}=this.state
  let newData = {
    id: data.length + 1,
    title: title,
    status:'predding '
  }
  data.push(newData)
  this.setState(data)
}


  render() {
    return (
      <div>
        <Container>

          <InputTable onAdd={this.onAdd} />
          <TableInfo onChangeStatus={this.onChangeStatus} data={this.state.data}/>

        </Container>

      </div>
    )
  }
}

